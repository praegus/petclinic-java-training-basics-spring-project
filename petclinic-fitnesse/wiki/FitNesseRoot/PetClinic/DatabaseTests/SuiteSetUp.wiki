!*> Setup jdbcSlim
|import                 |
|six42.fitnesse.jdbcslim|

!| define table type|
|SQLCommand|as Table|

*!
!*> Driver specific setup 
!path ${LibInstallPath}h2-*.jar

!|Define Properties|TestDatabase                     |
|key               |value                            |
|jdbcDriver        |com.mysql.jdbc.Driver            |
|DBURL             |jdbc:mysql://mysql:3306/petclinic|
|DBUSER            |root                             |
|DBPASSWORD        |petclinic                        |
|CMD               |                                 |

*!
!*> Stored cmds
!|define properties|deleteMyOwners                  |
|key               |value                           |
|.include          |TestDatabase                    |
|cmd               |DELETE FROM owners WHERE id > 10|

!|script         |SQLCommand|deleteMyOwners|
|$deleteMyOwners=|get fixture              |

!|define alias                         |
|Delete My Owers|table: $deleteMyOwners|

*!
!*> Create test data

!|SQLCommand|TestDatabase|INSERT INTO owners VALUES (%Id%, '%Firstname%', '%Lastname%', '%Address%.', '%City%', '%Phone%')|
|Id         |Firstname   |Lastname          |Address               |City              |Phone              |Count?         |
|0          |Tom         |Tester            |Teststraat 1          |Amsterdam         |0201234567         |               |
|0          |Maarten-Jan |Testmans          |Teststraat 3          |Rotterdam         |0101234567         |               |

*!
