FROM anapsix/alpine-java
MAINTAINER praegus
COPY spring-petclinic/target/spring-petclinic-1.5.1.jar /home/app.jar
EXPOSE 8080
CMD ["java","-jar","/home/app.jar", "--server.port=8080"]
