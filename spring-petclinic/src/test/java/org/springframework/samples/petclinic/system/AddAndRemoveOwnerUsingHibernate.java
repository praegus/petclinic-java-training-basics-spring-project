package org.springframework.samples.petclinic.system;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.samples.petclinic.owner.*;
import org.springframework.samples.petclinic.service.EntityUtils;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AddAndRemoveOwnerUsingHibernate {

    private static final int TEST_OWNER_ID = 1;

    @Autowired
    private OwnerRepository owners;
    private PetRepository pets;


    @Test
    public void shouldInsertOwnerWithPet() {
        Collection<Owner> owners = this.owners.findByLastName("Schultz");
        int found = owners.size();

        Owner owner = new Owner();
        owner.setFirstName("Sam");
        owner.setLastName("Schultz");
        /* Implement the rest!

        this.owners.save(owner);

        assertThat(owner.getId().longValue()).isNotEqualTo(0);

        owners = this.owners.findByLastName("Schultz");
        assertThat(owners.size()).isEqualTo(found + 1);
        */
    }

    @Test
    public void shouldRemoveOwnerAndPets() {
        Collection<Owner> owners = this.owners.findByLastName("Schultz");
        for(Owner owner : owners) {
            /* needs implementation too! */
        }
    }
}
