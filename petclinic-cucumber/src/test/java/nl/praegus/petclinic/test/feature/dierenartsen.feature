#language: nl
Functionaliteit: Testen PetClinic app

  Scenario: Het dierenartsen overzicht
    Gegeven ik op de homepage ben
    En ik navigeer naar "Veterinarians"
    Dan vind ik dierenarts "Linda Douglas" met specialiteit "dentistry surgery"