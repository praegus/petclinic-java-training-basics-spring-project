package nl.praegus.petclinic.test.stepdefinition;

import cucumber.api.java.nl.En;
import org.mockserver.client.server.MockServerClient;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.mockserver.model.HttpRequest.request;

public class Mockserver extends CucumberSteps {
    private final int port = 1337;
    private MockServerClient mockServerClient = ClientAndServer.startClientAndServer(port);

    public static void main(String[] args) {
        Mockserver mockserver = new Mockserver();
        mockserver.mockOptions();
        mockserver.mockGetCampings("Fort Oranje");
    }

    @En("^ik camping \"([^\"]*)\" gemockt heb$")
    public void ikCampingGemocktHeb(String camping) throws Throwable {
        mockOptions();
        mockGetCampings(camping);
    }

    public void mockOptions() {
        HttpRequest request = request().withMethod("OPTIONS").withPath("/campings");
        getClient()
            .when(request)
            .respond(HttpResponse
                .response()
                .withHeader("Access-Control-Allow-Headers", "authorization, content-type")
                .withHeader("Access-Control-Allow-Origin", "http://localhost:8000")
                .withHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS")
                .withHeader("Content-Type", "application/json")
                .withStatusCode(200));

    }

    public void mockGetCampings(String camping) {
        HttpRequest request = request().withMethod("GET").withPath("/campings");
        getClient()
            .when(request)
            .respond(HttpResponse
                .response()
                .withBody(String.format(GET_CAMPINGS, camping))
                .withHeader("Access-Control-Allow-Origin", "http://localhost:8000")
                .withHeader("Content-Type", "application/json")
                .withStatusCode(200));
    }

    protected MockServerClient getClient() {
        return this.mockServerClient;
    }

    public List<HttpRequest> getHttpRequestsFromEndpoint(String endpoint, String method, int verwachtAantal) throws InterruptedException {
        HttpRequest request = request().withMethod("POST").withPath("");
        return Arrays.asList(mockServerClient.retrieveRecordedRequests(request));
    }

    public void mockTechnischeFout(String method, String endpoint) throws IOException {
        HttpRequest request = HttpRequest.request().withMethod(method).withPath(endpoint);
        this.getClient().when(request).respond(HttpResponse.response().withStatusCode(Integer.valueOf(500)));
    }

    private static final String GET_CAMPINGS = "{\n"
        + "    \"campings\": [\n"
        + "        {\n"
        + "            \"pricePerNight\": 37.43,\n"
        + "            \"index\": 31,\n"
        + "            \"_id\": \"5584546aa1dde4eb510a306a\",\n"
        + "            \"hasAnimation\": \"yes\",\n"
        + "            \"stars\": 4,\n"
        + "            \"name\": \"%s\",\n"
        + "            \"hasPool\": \"yes\",\n"
        + "            \"country\": \"BE\",\n"
        + "            \"city\": \"Foxworth\"\n"
        + "        },\n"
        + "        {\n"
        + "            \"pricePerNight\": 37.43,\n"
        + "            \"index\": 31,\n"
        + "            \"_id\": \"5584546aa1dde4eb510a306a\",\n"
        + "            \"hasAnimation\": \"yes\",\n"
        + "            \"stars\": 4,\n"
        + "            \"name\": \"Nog een camping\",\n"
        + "            \"hasPool\": \"yes\",\n"
        + "            \"country\": \"BE\",\n"
        + "            \"city\": \"Foxworth\"\n"
        + "        }\n"
        + "    ]\n"
        + "}";

}
