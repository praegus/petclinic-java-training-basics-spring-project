package nl.praegus.petclinic.test.stepdefinition;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.File;
import java.util.Arrays;

public class CucumberSteps {
    private WebDriver driver;

    public WebDriver getWebdriver() {
        if (driver == null) {
            if(null == System.getProperty("webdriver.chrome.driver")) {
                System.setProperty("webdriver.chrome.driver", "petclinic-cucumber/tools/chromedriver_2.33_linux");
            }
//            String chromeLocation = "C:/Program Files (x86)/Google/Chrome/Application/chrome.exe";
            ChromeOptions options = new ChromeOptions();
//            options.setBinary(new File(chromeLocation));
            options.addArguments(Arrays.asList("chrome.switches", "--disable-extensions"));
            driver = new ChromeDriver(options);
            return driver;
        } else {
            return driver;
        }
    }

}
