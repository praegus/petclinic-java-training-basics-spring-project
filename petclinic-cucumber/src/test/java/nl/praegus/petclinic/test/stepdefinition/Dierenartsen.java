package nl.praegus.petclinic.test.stepdefinition;

import cucumber.api.java.After;
import cucumber.api.java.nl.Dan;
import cucumber.api.java.nl.En;
import cucumber.api.java.nl.Gegeven;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class Dierenartsen extends CucumberSteps {
    @Gegeven("^ik op de homepage ben$")
    public void ikOpDeHomepageBen() throws Throwable {
        getWebdriver().get("http://localhost:8080");
    }

    @En("^ik navigeer naar \"([^\"]*)\"$")
    public void ikNavigeerNaar(String item) throws Throwable {
        WebElement menuItem = getWebdriver().findElement(By.xpath("//*[text()='" + item + "']"));
        menuItem.click();
    }

    @Dan("^vind ik dierenarts \"([^\"]*)\" met specialiteit \"([^\"]*)\"$")
    public void vindIkDierenartsMetSpecialiteit(String vet, String specialty) throws Throwable {
        WebElement cellToCheck = getWebdriver().findElement(By.xpath("//td[text()='" + vet + "']/following-sibling::td"));
        assertThat(cellToCheck.getText().trim()).isEqualTo(specialty);
    }

    @After
    public void afterScenario() {
        getWebdriver().close();
    }
}
