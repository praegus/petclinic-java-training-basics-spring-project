package nl.praegus.petclinic.test;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = "src/test/java/nl/praegus/petclinic/test/feature",
		glue={"nl.praegus.petclinic.test.stepdefinition"}
		)

public class TestRunner {

}
